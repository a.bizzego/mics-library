import numpy as np

def process_discipline(data, discipline_indicators, is_physical_ok):
    
    data['isPhysicalOk'] = data[is_physical_ok]
    data['isPhysicalOk'].replace({2:0, 9:np.nan, 8:np.nan}, inplace=True)
    
    #extract cd indexes
    data_disc = data.loc[:, discipline_indicators]
    #recode No = 2 : 0, Missing = 9 : np.nan
    data_disc.replace({2:0, 9:np.nan}, inplace=True)
    
    UNICEF_score_coeff = np.array([1, #'TookAway',
                                   1, #ExplainedWrong
                                   3, #ShookChild
                                   2, #ShoutedYelled
                                   1, #ElseToDo
                                   3, #SpankedBare
                                   3, #HitElseWithSmt
                                   2, #CalledChildDumb
                                   4, #HitFace
                                   3, #HitHand
                                   4]) #BeatHard/WithImpl
    #most severe strategy
    data_disc_scored = data_disc * UNICEF_score_coeff #multiply by the gravity
    
    data_most_severe = data_disc_scored.max(axis = 1)
    data['MostSevere'] = data_most_severe
    
    #score_strategies
    #non violent
    data_nv = data_disc.iloc[:, np.where(UNICEF_score_coeff==1)[0]].max(axis=1)
    data['NonViolent'] = (data_nv==1).astype(int)
    
    #psycho
    data_psyc = data_disc.iloc[:, np.where(UNICEF_score_coeff==2)[0]].max(axis=1)
    data['Psychological'] = (data_psyc==1).astype(int)
    
    #physical
    data_phy = data_disc.iloc[:, np.where(UNICEF_score_coeff==3)[0]].max(axis=1)
    data['Physical'] = (data_phy==1).astype(int)
    
    #physical
    data_sevphy = data_disc.iloc[:, np.where(UNICEF_score_coeff==4)[0]].max(axis=1)
    data['SeverePhysical'] = (data_sevphy==1).astype(int)
    
    #remove rows all nan
    return(data)

def process_caregiving(data, careg_cols, out_col):
    
    #select only the indicators of the activity
    data_active = data.loc[:, careg_cols]
    data_active.replace({'A':1, 'B':1, 'X':1, 'Y':1, '?':np.nan}, inplace=True)

    #compute the caregiving score:
    #0: no one
    #1: another only
    #2: one parent only
    #3: one parent and another
    #4: both parents
    #5: both parents and another
    caregiving_score = (data_active * np.array([2, 2, 1, 0])).sum(axis=1, min_count=1)
    data[out_col] = caregiving_score
    return(data)

def process_mfo_caregiver(data, acronyms, out_colname):
    data_active = data.loc[:, acronyms]
    
    #find rows with no answers
    data_active = (data_active.isna() == False).astype(int)
    
    sum_rows = np.sum(data_active, axis=1)
    idx_no_answer = np.where(sum_rows == 0)[0]
    
    #0: no one; 1: only mother, 3: only father, 5: only other, 
    #4: mother and father; 6: mother and other; 8: father and other; 9: mother father and other
    out_active = data_active * np.array([1, 3, 5, 0])
    out_active = out_active.sum(axis=1) 
    
    #set rows with no answers to nan
    out_active.iloc[idx_no_answer] = np.nan
    
    #0: no one
    #1: another only
    #2: one parent only
    #3: one parent and another
    #4: both parents
    #5: both parents and another
    score_active = data_active * np.array([2, 2, 1, 0])
    score_active = score_active.sum(axis=1) 
    
    #set rows with no answers to nan
    score_active.iloc[idx_no_answer] = np.nan
    
#    data[out_colname] = out_active
    data[f'{out_colname}_score'] = score_active
    data.drop(acronyms, axis=1, inplace=True)
    return(data)