import os
import pandas as pd
import numpy as np
from config import DATA_PATH
from mics_library.utils import remove_rows_all_nan

#%% function to process the caregiving indicators
def process_caregiving(data, acronyms, out_colname):
    
    #select only the indicators of the activity
    data_active = data.loc[:, acronyms]
    
    #compute the caregiving score:
    #0: no one
    #1: another only
    #2: one parent only
    #3: one parent and another
    #4: both parents
    #5: both parents and another
    caregiving_score = (data_active * np.array([2, 2, 1, 0])).sum(axis=1, min_count=1)
    
#    data[out_colname] = np.nan
    
    data[out_colname] = caregiving_score
    data.drop(acronyms, axis=1, inplace=True)
    return(data)
    
'''
snippet:
#get the combination of caregivers that do the activity with the child
#0: no one; 1: only mother, 3: only father, 5: only other, 
#4: mother and father; 6: mother and other; 8: father and other; 9: mother father and other
#caregiver_combination = (data_active * np.array([1, 3, 5, 0])).sum(axis=1, min_count=1)
'''
    
#%%
caregiving_indicators = ['EC7AA', 'EC7AB', 'EC7AX', 'EC7AY', 
                         'EC7BA', 'EC7BB', 'EC7BX', 'EC7BY', 
                         'EC7CA', 'EC7CB', 'EC7CX', 'EC7CY', 
                         'EC7DA', 'EC7DB', 'EC7DX', 'EC7DY', 
                         'EC7EA', 'EC7EB', 'EC7EX', 'EC7EY', 
                         'EC7FA', 'EC7FB', 'EC7FX', 'EC7FY',
                         'DB1']

#%%
datafile = os.path.join(DATA_PATH, 'raw.csv')

#%%load data
data = pd.read_csv(datafile, index_col=0)

#get keys
keys = data[['HHID', 'HLID', 'mother_HLID', 'country', 'caretaker_HLID']]

#get development indicators
data = data[caregiving_indicators]

#%% recode
data.replace({'A':1, 'B':1, 'X':1, 'Y':1, '?':np.nan}, inplace=True)

#cast to numeric
data = data.astype(float)

#%% process

#remove rows all nan
#probably the module was not asked
data = remove_rows_all_nan(data)

#process each activity to compute the activity score
acronyms_EC7A = ['EC7AA', 'EC7AB', 'EC7AX', 'EC7AY']
data = process_caregiving(data, acronyms_EC7A, 'ReadBook')

acronyms_EC7B = ['EC7BA', 'EC7BB', 'EC7BX', 'EC7BY']
data = process_caregiving(data, acronyms_EC7B, 'TellStories')

acronyms_EC7C = ['EC7CA', 'EC7CB', 'EC7CX', 'EC7CY']
data = process_caregiving(data, acronyms_EC7C, 'SingSongs')

acronyms_EC7D = ['EC7DA', 'EC7DB', 'EC7DX', 'EC7DY']
data = process_caregiving(data, acronyms_EC7D, 'TakeOutside')

acronyms_EC7E = ['EC7EA', 'EC7EB', 'EC7EX', 'EC7EY']
data = process_caregiving(data, acronyms_EC7E, 'PlayWith')

acronyms_EC7F = ['EC7FA', 'EC7FB', 'EC7FX', 'EC7FY']
data = process_caregiving(data, acronyms_EC7F, 'NameCount')

#compute cognitive caregiving score (no nans allowed --> min_count=3)
data['cognitive_caregiving'] = data[['ReadBook', 'TellStories', 'NameCount']].sum(axis=1, min_count = 3)

#compute socio-emotional caregiving score (no nans allowed --> min_count=3)
data['socioemotional_caregiving'] = data[['SingSongs', 'TakeOutside', 'PlayWith']].sum(axis=1, min_count = 3)

#compute total caregiving score (no nans allowed --> min_count = 2)
data['total_caregiving'] = data[['cognitive_caregiving', 'socioemotional_caregiving']].sum(axis=1, min_count = 2)

data.boxplot('total_caregiving', by = 'DB1')
