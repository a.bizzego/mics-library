import mics_library
import os
from mics_library.preview import check_values

ROOTDIR = '/home/bizzego/UniTn/data/mics/original' 

mics_library.set_rootdir(ROOTDIR)

ROUND = 5

select_indicators = {'hh': ['HH1']}

dataframes = check_values(micsround=ROUND, indicators=select_indicators, swap_indicators={}, ignorecase=True)


CHECKDIR = '/home/bizzego/tmp'

#this will create a folder for each questionnaire
#and a csv file (inside the folder) for each item (of the questionnaire)
for quest in dataframes.keys():
    dataframes_quest = dataframes[quest]
    os.makedirs(os.path.join(CHECKDIR, quest), exist_ok=True)
    
    for ind in dataframes_quest.keys():
        dataframes_quest[ind].to_csv(os.path.join(CHECKDIR, quest, f'{ind}.csv'))


# ### `swap_indicators`
# 
# The `swap_indicators` parameter in the `mics_library` is used to deal with the different acronyms that are used by each country to indicate the SAME question.
# The `swap_indicators` parameter should be a dictionary:
# 
# `{'questionnaire': 
#     {'COUNTRY': {'TARGET_INDICATOR' : 'USED_INDICATOR', 
#                  ...}        
#      ...}
#  }`
#  
#  Where `TARGET_INDICATOR` is the acronym of item that is used by the other countries and `USED_INDICATOR` is the acronym of the same item in the specific `COUNTRY`.
#  
#  The `mics_library` already includes a database of indicators that should be swapped for each MICS round, which will be updated as we find more inconsistencies. 

# In[7]:


from mics_library.swap_indicators import swap_indicators

display(swap_indicators[3])


#  To notify more indicators that should be swapped, please open an Issue on the dedicated [gitlab]() page.
#  
#  When a different indicator is used for a specific country, the column `used_indicator` will show the true `USED_INDICATOR` (instead of the `TARGET_INDICATOR`).

# ### `ignorecase`
# The `ignorecase` parameter is used to force the `mics_library` to treat acronyms composed by the same characters but different case, as the same acronym.
# If `ignorecase=True`, `AG2` and `ag2` or `Ag2` (etc etc) will be considered as the same item.

# In[ ]:



