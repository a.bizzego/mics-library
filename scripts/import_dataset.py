import mics_library
import os
from mics_library.loaders import import_dataset

ROOTDIR = '/home/bizzego/UniTn/data/mics/original' 

mics_library.set_rootdir(ROOTDIR)

ROUND = 6

select_indicators = {'hh': ['HH1'], 
                     'hl': ['HH1'],
                     'wm': ['HH1'],
                     'mn': ['HH1'],
                     'ch': ['HH1'],
                     'bh': ['HH1'],
                     'fs': ['HH1']}

dataset = import_dataset(ROUND, select_indicators, swap_indicators={},
                         recoding_dictionary={}, ignorecase=True)    

#%%
from mics_library.loaders import merge_questionnaires

data, keys = merge_questionnaires(dataset)


OUT_DIR = '/path/to/datasets'
data.to_csv(os.path.join(OUT_DIR, 'data.csv'))
keys.to_csv(os.path.join(OUT_DIR, 'keys.csv'))