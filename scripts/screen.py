import mics_library
from mics_library.preview import screen
import os

ROOTDIR = '/home/bizzego/UniTn/data/mics/original'

mics_library.set_rootdir(ROOTDIR)

#%%
ROUND = 6
TARGET_QUESTIONNAIRES = ['bh', 
                         'ch',
                         'hl',
                         'fg',
                         'fs',
                         'hh',
                         'hl',
                         'mn',
                         'tn',
                         'wm']

dataframes = screen(ROUND, questionnaires=TARGET_QUESTIONNAIRES, ignorecase=True)

SCREENDIR = '/home/bizzego/UniTn/data/mics/screen/MICS6'

for quest in TARGET_QUESTIONNAIRES:
    dataframes[quest].to_csv(os.path.join(SCREENDIR, f'{quest}.csv'))